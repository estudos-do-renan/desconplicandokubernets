valores = []
while True:
    num = int(input('Digite um valor: '))
    if num in valores:
        print('Lista já contém o valor digitado.')
    else:
        valores.append(num)
    cont = str(input('Deseja continuar? [S/N]: '))
    if cont == 'N' or cont == 'n':
        break
    if cont != 's' and cont != 'S':
        print('Erro')
        cont = str(input('Deseja continuar? [S/N]: '))
valores.sort()
print(valores)


