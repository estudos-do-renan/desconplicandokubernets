lista = []
lista_par = []
lista_impar = []

while True:
    num = int(input('Digite um valor: '))
    lista.append(num)
    if num % 2 == 0:
        lista_par.append(num)
    else:
        lista_impar.append(num)
    cont = str(input('Deseja continuar? [S/N]: ')).upper()
    if cont == 'N':
        break
    if cont != 'S':
        print('Erro')

print(f'lista comum é: {lista}')
print(f'Lista par é: {lista_par}')
print(f'Lista impar é: {lista_impar}')
