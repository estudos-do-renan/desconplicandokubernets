times = ['Flamengo', 'Internacional', 'Atlético-MG', 'São Paulo', 'Fluminense', 'Grêmio', 'Palmeiras', 'Santos', 'Atlético-PR', 'Bragantino', 'Ceará', 'Cortinthians', 'Atlético-GO', 'Bahia', 'Sport', 'Fortaleza', 'Vasco', 'Góias', 'Coritiba', 'Botafogo']
print(f'A) Os cinco primeiros colocados são {times[0:5]}')
print(f'B) Os ultimos colocados são {times[15:]}')
print(f'C) {sorted(times)}')
print(f'D) O Corinthians está na {times.index("Cortinthians")+1}ª posição')