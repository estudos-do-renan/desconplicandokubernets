numgrande = ('zero', 'um', 'dois', 'três', 'quatro', 'cinco', 'seis', 'sete', 'oito', 'nove', 'dez', 'onze', 'doze', 'treze', 'catorze', 'quinze', 'dezesseis', 'dezesete', 'dezoito', 'dezenove', 'vinte')
cont = 's'
while cont == 's':
    while True:
        num = int(input('Digite o número desejado: '))
        if num < 0 or num > 20:
            print('Erro, Tente novamente.')
        else:
            break
    print(f'o número é {numgrande[num]}')
    cont = str(input('Deseja continuar'))
    if cont == 'f':
        break